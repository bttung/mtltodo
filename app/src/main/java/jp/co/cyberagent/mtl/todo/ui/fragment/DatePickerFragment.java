package jp.co.cyberagent.mtl.todo.ui.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by a13561 on 2015/07/14.
 */
public class DatePickerFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

    public static final String TAG = "DATE_PICKER";

    public static interface OnCompleteListener {
        public abstract void onSelectedDate(int year, int monthOfYear, int dayOfMonth);
    }

    private OnCompleteListener mOnCompleteListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            this.mOnCompleteListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.mOnCompleteListener.onSelectedDate(year, monthOfYear, dayOfMonth);
    }
}
