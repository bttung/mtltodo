package jp.co.cyberagent.mtl.todo.util;

import java.util.Calendar;

import jp.co.cyberagent.mtl.todo.model.Deadline;

/**
 * Created by a13561 on 2015/07/13.
 */
public class TodoTimeConverter {

    public static int TODO_PERIOD_DAY = 1;

    public static Deadline getCurrentTime() {
        Deadline time = new Deadline();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, TODO_PERIOD_DAY);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        time.setYear(year);
        time.setMonth(month);
        time.setDay(day);
        time.setHour(hour);
        time.setMinute(minute);

        return time;
    }

    public static String getDateStr(Deadline deadline) {
        return String.format("%1$04d/%2$02d/%3$02d",
                deadline.getYear(), deadline.getMonth(), deadline.getDay());
    }

    public static String getTimeStr(Deadline deadline) {
        return String.format("%1$02d:%2$02d",
                deadline.getHour(), deadline.getMinute());
    }

    public static String getFullStr(Deadline deadline) {
        return (getDateStr(deadline) + " " + getTimeStr(deadline));
    }

    public static Deadline getDeadline(String content) {
        Deadline deadline = new Deadline();

        String sub = content.substring(0, 4);
        deadline.setYear(Integer.parseInt(sub));

        sub = content.substring(5, 7);
        deadline.setMonth(Integer.parseInt(sub));

        sub = content.substring(8, 10);
        deadline.setDay(Integer.parseInt(sub));

        sub = content.substring(11, 13);
        deadline.setHour(Integer.parseInt(sub));

        sub = content.substring(14, 15);
        deadline.setMinute(Integer.parseInt(sub));

        return deadline;
    }
}
