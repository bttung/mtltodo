package jp.co.cyberagent.mtl.todo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import jp.co.cyberagent.mtl.todo.R;
import jp.co.cyberagent.mtl.todo.db.TodoHelper;
import jp.co.cyberagent.mtl.todo.model.Deadline;
import jp.co.cyberagent.mtl.todo.model.Todo;
import jp.co.cyberagent.mtl.todo.ui.fragment.DatePickerFragment;
import jp.co.cyberagent.mtl.todo.ui.fragment.PriorityPickerFragment;
import jp.co.cyberagent.mtl.todo.ui.fragment.TimePickerFragment;
import jp.co.cyberagent.mtl.todo.util.TodoTimeConverter;

/**
 * Created by a13561 on 2015/07/13.
 */
public class TodoInputActivity extends FragmentActivity implements
        DatePickerFragment.OnCompleteListener,
        TimePickerFragment.OnCompleteListener,
        PriorityPickerFragment.OnCompleteListener{

    private EditText mTitleEt;
    private EditText mDetailEt;
    private TextView mDayTv;
    private TextView mTimeTv;
    private TextView mPriorityExplanTv;
    private TextView mPriorityTv;
    private CheckBox mIsDoneCheckbox;
    private FloatingActionButton mCancelBt;
    private FloatingActionButton mDeleteBt;
    private FloatingActionButton mUpdateBt;
    private FloatingActionButton mSaveBt;

    private TodoHelper mTodoHelper;
    private String mTodoId;
    private Deadline mDeadline;
    private Todo mTodo;

    private String mDone;
    private String mUndone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_input);

        mTodoHelper = new TodoHelper(getApplicationContext());
        mTodo = new Todo();
        mDeadline = TodoTimeConverter.getCurrentTime();

        setupUI();
        SetupEdittingTodoContent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTodoHelper.close();
    }

    private void setupUI() {
        mTitleEt = (EditText) findViewById(R.id.todo_edit_title);
        mDetailEt = (EditText) findViewById(R.id.todo_edit_detail);
        mDayTv = (TextView) findViewById(R.id.todo_deadline_date_tv);
        mTimeTv = (TextView) findViewById(R.id.todo_deadline_time_tv);
        mPriorityExplanTv = (TextView) findViewById(R.id.todo_priority_explan_tv);
        mPriorityTv = (TextView) findViewById(R.id.todo_priority_tv);
        mIsDoneCheckbox = (CheckBox) findViewById(R.id.todo_edit_done);
        mCancelBt = (FloatingActionButton) findViewById(R.id.todo_cancel_fab);
        mDeleteBt = (FloatingActionButton) findViewById(R.id.todo_delete_fab);
        mUpdateBt = (FloatingActionButton) findViewById(R.id.todo_update_fab);
        mSaveBt = (FloatingActionButton) findViewById(R.id.todo_save_fab);

        mDone = getResources().getString(R.string.todo_is_done);
        mUndone = getResources().getString(R.string.todo_is_unDone);

        mDayTv.setText(TodoTimeConverter.getDateStr(mDeadline));
        mTimeTv.setText(TodoTimeConverter.getTimeStr(mDeadline));
        mPriorityTv.setText(String.valueOf(PriorityPickerFragment.PRIORITY_MIN));

        mDayTv.setOnClickListener(mOpenDateSelectDialogHandler);
        mTimeTv.setOnClickListener(mOpenTimeSelectDialogHandler);
        mPriorityExplanTv.setOnClickListener(mOpenPriorityDialogHandler);
        mPriorityTv.setOnClickListener(mOpenPriorityDialogHandler);
        mIsDoneCheckbox.setOnClickListener(mDoneCheckBoxChangedHandler);

        mCancelBt.setOnClickListener(mCancelHandler);
        mDeleteBt.setOnClickListener(mDeleteHandler);
        mUpdateBt.setOnClickListener(mUpdateHandler);
        mSaveBt.setOnClickListener(mAddHandler);
    }

    private void SetupEdittingTodoContent() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        mTodoId = bundle.getString(MainActivity.TODO_ITEM_ID);
        mTodoId = bundle.getString(MainActivity.TODO_ITEM_ID);
        if (mTodoId == null || mTodoId.length() == 0) {
            return;
        }

        mTodo = (Todo) intent.getSerializableExtra(MainActivity.TODO_ITEM_VALUE);
        if (mTodo == null) {
            return;
        }

        mDeadline = TodoTimeConverter.getDeadline(mTodo.getDeadline());
        mTitleEt.setText(mTodo.getTitle());
        mDetailEt.setText(mTodo.getDetail());
        mDayTv.setText(TodoTimeConverter.getDateStr(mDeadline));
        mTimeTv.setText(TodoTimeConverter.getTimeStr(mDeadline));
        mPriorityTv.setText(String.valueOf(mTodo.getPriority()));
        mIsDoneCheckbox.setChecked(mTodo.getDone().equals(mDone));

        mDeleteBt.setVisibility(View.VISIBLE);
        mSaveBt.setVisibility(View.INVISIBLE);
        mUpdateBt.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener mOpenDateSelectDialogHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment datePicker = new DatePickerFragment();
            datePicker.show(getSupportFragmentManager(), DatePickerFragment.TAG);
        }
    };

    private View.OnClickListener mOpenTimeSelectDialogHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment timePicker = new TimePickerFragment();
            timePicker.show(getSupportFragmentManager(), TimePickerFragment.TAG);
        }
    };

    private View.OnClickListener mDoneCheckBoxChangedHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mTodo.setDone(mIsDoneCheckbox.isChecked() ? mDone : mUndone);
        }
    };

    private View.OnClickListener mOpenPriorityDialogHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment priorityPicker = new PriorityPickerFragment();
            priorityPicker.show(getSupportFragmentManager(), PriorityPickerFragment.TAG);
        }
    };

    @Override
    public void onSelectedDate(int year, int monthOfYear, int dayOfMonth) {
        mDeadline.setYear(year);
        mDeadline.setMonth(monthOfYear);
        mDeadline.setDay(dayOfMonth);
        mDayTv.setText(TodoTimeConverter.getDateStr(mDeadline));
    }

    @Override
    public void onSelectedTime(int hourOfDay, int minute) {
        mDeadline.setHour(hourOfDay);
        mDeadline.setMinute(minute);
        mTimeTv.setText(TodoTimeConverter.getTimeStr(mDeadline));
    }

    @Override
    public void OnSelectedPriority(int priority) {
        mTodo.setPriority(priority);
        mPriorityTv.setText(String.valueOf(priority));
    }

    private View.OnClickListener mAddHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTodoHelper == null) {
                return;
            }

            Todo todo = new Todo();
            todo.setTitle(mTitleEt.getText().toString());
            todo.setDetail(mDetailEt.getText().toString());
            todo.setDeadline(TodoTimeConverter.getFullStr(mDeadline));
            todo.setPriority(Integer.parseInt(mPriorityTv.getText().toString()));
            todo.setDone(mIsDoneCheckbox.isChecked() ? mDone : mUndone);
            mTodoHelper.insert(todo);

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener mCancelHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTodoHelper == null) {
                return;
            }

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener mDeleteHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTodoId == null || mTodoId.length() == 0) {
                return;
            }

            mTodoHelper.delete(mTodoId);

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener mUpdateHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTodoHelper == null) {
                return;
            }

            mTodo.setTitle(mTitleEt.getText().toString());
            mTodo.setDetail(mDetailEt.getText().toString());
            mTodo.setDeadline(TodoTimeConverter.getFullStr(mDeadline));
            mTodoHelper.update(mTodoId, mTodo);

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    };
}
