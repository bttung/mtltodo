package jp.co.cyberagent.mtl.todo.model;

import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by a13561 on 2015/07/13.
 */
public class Todo implements BaseColumns, Serializable {
    public static final String TODO_TABLE_NAME = "todo";

    public static final String COLUMN_NAME_ID = "_id";
    public static final String COLUMN_NAME_TODO_TITLE = "title";
    public static final String COLUMN_NAME_TODO_DETAIL = "detail";
    public static final String COLUMN_NAME_TODO_PRIORITY = "priority";
    public static final String COLUMN_NAME_TODO_DEADLINE = "deadline";
    public static final String COLUMN_NAME_TODO_DONE = "done";

    private String mTitle;
    private String mDetail;
    private int mPriority;
    private String mDeadline;
    private String mDone;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String detail) {
        mDetail = detail;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public String getDeadline() {
        return mDeadline;
    }

    public void setDeadline(String deadline) {
        mDeadline = deadline;
    }

    public String getDone() {
        return mDone;
    }

    public void setDone(String done) {
        mDone = done;
    }
}
