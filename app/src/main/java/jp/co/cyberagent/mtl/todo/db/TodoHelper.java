package jp.co.cyberagent.mtl.todo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import jp.co.cyberagent.mtl.todo.model.Todo;

/**
 * Created by a13561 on 2015/07/13.
 */
public class TodoHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Todo.db";

    private static final String TODO_TABLE_CREATE =
            "CREATE TABLE " + Todo.TODO_TABLE_NAME + " (" +
                    Todo._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Todo.COLUMN_NAME_TODO_TITLE + " TEXT NOT NULL, " +
                    Todo.COLUMN_NAME_TODO_DETAIL + " TEXT NOT NULL, " +
                    Todo.COLUMN_NAME_TODO_PRIORITY + " TEXT, " +
                    Todo.COLUMN_NAME_TODO_DEADLINE + " INTEGER, " +
                    Todo.COLUMN_NAME_TODO_DONE + " INTEGER)";

    private static final String TODO_TABLE_DELETE =
            "DROP TABLE IF EXISTS " + Todo.TODO_TABLE_NAME;

    public TodoHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TODO_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TODO_TABLE_DELETE);
        onCreate(db);
    }

    public long insert(Todo todo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = getContentValues(todo);

        return db.insertOrThrow(Todo.TODO_TABLE_NAME, null, values);
    }

    public int update(String id, Todo todo) {
        SQLiteDatabase db = getWritableDatabase();
        String selection = Todo.COLUMN_NAME_ID + "=" + id;
        ContentValues values = getContentValues(todo);

        return db.update(Todo.TODO_TABLE_NAME, values, selection, null);
    }

    private ContentValues getContentValues(Todo todo) {
        ContentValues values = new ContentValues();
        values.put(Todo.COLUMN_NAME_TODO_TITLE, todo.getTitle());
        values.put(Todo.COLUMN_NAME_TODO_TITLE, todo.getTitle());
        values.put(Todo.COLUMN_NAME_TODO_DETAIL, todo.getDetail());
        values.put(Todo.COLUMN_NAME_TODO_PRIORITY, todo.getPriority());
        values.put(Todo.COLUMN_NAME_TODO_DEADLINE, todo.getDeadline());
        values.put(Todo.COLUMN_NAME_TODO_DONE, todo.getDone());

        return  values;
    }

    public Cursor readAll() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Todo.TODO_TABLE_NAME, null, null, null, null, null, null);
        return cursor;
    }

    public Cursor sortBy(QueryType queryType) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Todo.TODO_TABLE_NAME, null,
                                    null, null, null, null, queryType.toString());
        return cursor;
    }

    public int delete(String id) {
        SQLiteDatabase db = getWritableDatabase();
        String selection = Todo.COLUMN_NAME_ID + "=" + id;
        return db.delete(Todo.TODO_TABLE_NAME, selection, null);
    }

    public int deleteAll() {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(Todo.TODO_TABLE_NAME, null, null);
    }
}
