package jp.co.cyberagent.mtl.todo.db;

import jp.co.cyberagent.mtl.todo.model.Todo;

/**
 * Created by a13561 on 2015/07/15.
 */

public enum QueryType {

    CREATED_TIME_ASC(Todo.COLUMN_NAME_ID),
    TITLE_ASC(Todo.COLUMN_NAME_TODO_TITLE),
    DETAIL_ASC(Todo.COLUMN_NAME_TODO_DETAIL),
    DEADLINE_ASC(Todo.COLUMN_NAME_TODO_DEADLINE),
    PRIORITY_ASC(Todo.COLUMN_NAME_TODO_PRIORITY),
    IS_DONE_ASC(Todo.COLUMN_NAME_TODO_DONE),
    CREATED_TIME_DESC(Todo.COLUMN_NAME_ID + " DESC"),
    TITLE_DESC(Todo.COLUMN_NAME_TODO_TITLE + " DESC"),
    DETAIL_DESC(Todo.COLUMN_NAME_TODO_DETAIL + " DESC"),
    DEADLINE_DESC(Todo.COLUMN_NAME_TODO_DEADLINE + " DESC"),
    PRIORITY_DESC(Todo.COLUMN_NAME_TODO_PRIORITY + " DESC"),
    IS_DONE_DESC(Todo.COLUMN_NAME_TODO_DONE + " DESC");

    private final String mQueryType;

    private QueryType(String s) {
        this.mQueryType = s;
    }

    @Override
    public String toString() {
        return mQueryType;
    }
}