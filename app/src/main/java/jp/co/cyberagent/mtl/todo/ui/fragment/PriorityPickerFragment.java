package jp.co.cyberagent.mtl.todo.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import jp.co.cyberagent.mtl.todo.R;

/**
 * Created by a13561 on 2015/07/14.
 */
public class PriorityPickerFragment extends DialogFragment {

    public static final String TAG = "PRIORITY_PICKER";
    public static final int PRIORITY_MIN = 1;
    public static final int PRIORITY_MAX = 5;

    public static interface OnCompleteListener {
        public abstract void OnSelectedPriority(int priority);
    }

    private NumberPicker mPriorityPicker;
    private OnCompleteListener mOnCompleteListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mOnCompleteListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.todo_priority_dialog, null, false);

        mPriorityPicker = (NumberPicker) view.findViewById(R.id.todo_priority_picker);
        mPriorityPicker.setMinValue(PRIORITY_MIN);
        mPriorityPicker.setMaxValue(PRIORITY_MAX);

        Resources resources = getResources();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(resources.getString(R.string.todo_priority_dialog_title));
        builder.setPositiveButton(resources.getString(R.string.todo_dialog_yes), mOnClickListener);
        builder.setNegativeButton(resources.getString(R.string.todo_dialog_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    private DialogInterface.OnClickListener mOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mOnCompleteListener.OnSelectedPriority(mPriorityPicker.getValue());
        }
    };
}
