package jp.co.cyberagent.mtl.todo.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.support.design.widget.FloatingActionButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import jp.co.cyberagent.mtl.todo.R;
import jp.co.cyberagent.mtl.todo.db.QueryType;
import jp.co.cyberagent.mtl.todo.db.TodoHelper;
import jp.co.cyberagent.mtl.todo.model.Todo;
import jp.co.cyberagent.mtl.todo.ui.fragment.TodoSortPickerFragment;


public class MainActivity extends FragmentActivity implements
        TodoSortPickerFragment.OnCompleteListener {

    public static final int INTENT_ADD_REQUEST_CODE = 1000;
    public static final int INTENT_ADD_EDIT_CODE = 2000;
    public static final String TODO_ITEM_ID = "TODO_ITEM_ID";
    public static final String TODO_ITEM_VALUE = "TODO_ITEM_VALUE";

    private static final String[] QUERY_FROM = {Todo._ID, Todo.COLUMN_NAME_TODO_TITLE, Todo.COLUMN_NAME_TODO_DETAIL,
            Todo.COLUMN_NAME_TODO_PRIORITY, Todo.COLUMN_NAME_TODO_DEADLINE, Todo.COLUMN_NAME_TODO_DONE};
    private static final int[] QUERY_TO = {R.id.todo_id, R.id.todo_title, R.id.todo_detail,
            R.id.todo_priority, R.id.todo_deadline, R.id.todo_done};

    private TodoHelper mTodoHelper;
    private SimpleCursorAdapter mCursorAdapter;
    private Cursor mCursor;

    private FloatingActionButton mAddBt;
    private FloatingActionButton mSortBt;
    private FloatingActionButton mDeleteAllBt;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTodoHelper = new TodoHelper(getApplicationContext());
        mCursor = mTodoHelper.readAll();
        mListView = (ListView) findViewById(R.id.todo_lv);

        mCursorAdapter = new SimpleCursorAdapter(this, R.layout.list_todo, mCursor, QUERY_FROM, QUERY_TO,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mListView.setAdapter(mCursorAdapter);
        mListView.setOnItemClickListener(itemClickListener);

        mAddBt = (FloatingActionButton) findViewById(R.id.todo_add_fab);
        mSortBt = (FloatingActionButton) findViewById(R.id.todo_sort_fab);
        mDeleteAllBt = (FloatingActionButton) findViewById(R.id.todo_delete_all_fab);

        mAddBt.setOnClickListener(mAddHandler);
        mSortBt.setOnClickListener(mSortHandler);
        mDeleteAllBt.setOnClickListener(mDeleteHandler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTodoHelper.close();
    }

    private View.OnClickListener mAddHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, TodoInputActivity.class);
            startActivityForResult(intent, INTENT_ADD_REQUEST_CODE);
        }
    };

    private View.OnClickListener mSortHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment sortTypePicker = new TodoSortPickerFragment();
            sortTypePicker.show(getSupportFragmentManager(), TodoSortPickerFragment.TAG);
        }
    };

    private View.OnClickListener mDeleteHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTodoHelper == null) {
                return;
            }

            mTodoHelper.deleteAll();
            mCursorAdapter.getCursor().requery();
        }
    };

    @Override
    public void OnSelectedSortType(int sortType) {

        QueryType queryType = QueryType.CREATED_TIME_ASC;
        switch (sortType) {
            case 0:
                queryType = QueryType.CREATED_TIME_ASC;
                break;
            case 1:
                queryType = QueryType.TITLE_ASC;
                break;
            case 2:
                queryType = QueryType.DETAIL_ASC;
                break;
            case 3:
                queryType = QueryType.DEADLINE_ASC;
                break;
            case 4:
                queryType = QueryType.PRIORITY_ASC;
                break;
            case 5:
                queryType = QueryType.IS_DONE_ASC;
                break;
            case 6:
                queryType = QueryType.CREATED_TIME_DESC;
                break;
            case 7:
                queryType = QueryType.TITLE_DESC;
                break;
            case 8:
                queryType = QueryType.DETAIL_DESC;
                break;
            case 9:
                queryType = QueryType.DEADLINE_DESC;
                break;
            case 10:
                queryType = QueryType.PRIORITY_DESC;
                break;
            case 11:
                queryType = QueryType.IS_DONE_DESC;
                break;
            default:
                break;
        }

        mCursor = mTodoHelper.sortBy(queryType);
        mCursorAdapter = new SimpleCursorAdapter(this, R.layout.list_todo, mCursor, QUERY_FROM, QUERY_TO,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mListView.setAdapter(mCursorAdapter);
    }

    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String todo_id = ((TextView) view.findViewById(R.id.todo_id)).getText().toString();
            String title = ((TextView) view.findViewById(R.id.todo_title)).getText().toString();
            String detail = ((TextView) view.findViewById(R.id.todo_detail)).getText().toString();
            String priority = ((TextView) view.findViewById(R.id.todo_priority)).getText().toString();
            String deadline = ((TextView) view.findViewById(R.id.todo_deadline)).getText().toString();
            String done = ((TextView) view.findViewById(R.id.todo_done)).getText().toString();

            Todo todo = new Todo();
            todo.setTitle(title);
            todo.setDetail(detail);
            todo.setPriority(Integer.parseInt(priority));
            todo.setDeadline(deadline);
            todo.setDone(done);

            Intent intent = new Intent(MainActivity.this, TodoInputActivity.class);
            intent.putExtra(TODO_ITEM_ID, todo_id);
            intent.putExtra(TODO_ITEM_VALUE, todo);
            startActivityForResult(intent, INTENT_ADD_EDIT_CODE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case INTENT_ADD_REQUEST_CODE:
            case INTENT_ADD_EDIT_CODE:
                if (resultCode == RESULT_OK) {
                    mCursorAdapter.getCursor().requery();
                }
                break;

            default:
                break;
        }
    }
}
