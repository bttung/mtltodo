package jp.co.cyberagent.mtl.todo.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import jp.co.cyberagent.mtl.todo.R;

/**
 * Created by a13561 on 2015/07/15.
 */
public class TodoSortPickerFragment extends DialogFragment {

    public static final String TAG = "SORT_TYPE";

    public static interface OnCompleteListener {
        public abstract void OnSelectedSortType(int sortType);
    }

    private OnCompleteListener mOnCompleteListener;
    private int mSelectedIndex;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mOnCompleteListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Resources resources = getResources();
        String sortTypeList[] = resources.getStringArray(R.array.todo_sort_type);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(resources.getString(R.string.todo_sort_title));
        builder.setSingleChoiceItems(sortTypeList, 0, mOnSelectedListener);
        builder.setPositiveButton(resources.getString(R.string.todo_dialog_yes), mOnDecidedListener);
        builder.setNegativeButton(resources.getString(R.string.todo_dialog_cancel), null);

        return builder.create();
    }

    private DialogInterface.OnClickListener mOnSelectedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mSelectedIndex = which;
        }
    };

    private DialogInterface.OnClickListener mOnDecidedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mOnCompleteListener.OnSelectedSortType(mSelectedIndex);
        }
    };
}
